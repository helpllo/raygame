#include "flag.h"

void Flag::ChangePosition(int x, int y) { box.x = x; box.y = y; }
void Flag::ChangePosition(Vector2 position) { box.x = position.x; box.y = position.y; }
void Flag::Draw() { DrawRectangleRec(box, color); }
Rectangle Flag::GetRectangle() { return box; }
