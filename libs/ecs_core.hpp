#pragma once
#include <bitset>
#include <vector>
#include "spdlog/spdlog.h"

const int MAX_COMP = 1024;
typedef std::bitset<MAX_COMP> CompomentMask;
typedef unsigned long long EntityID;
typedef unsigned int EntityVersion;
typedef unsigned int EntityIndex;

// GetCompomentId
static int s_componentCounter = 0;

template <class T> 
int GetID() {
  static int s_componentId = s_componentCounter++; // s_compomentId will be diffrent when diffrent T is used
  return s_componentId;
}

inline EntityID CreateEntityId(EntityIndex index, EntityVersion version)
{
  // Shift the index up 32, and put the version in the bottom
  return ((EntityID)version << 32) | ((EntityID)index);
}
inline EntityIndex GetEntityIndex(EntityID id)
{
  // Cast to a 32 bit int to get our version number (loosing the top 32 bits)
  return (EntityIndex)id;
}
inline EntityVersion GetEntityVersion(EntityID id)
{
  // Shift down 32 so we lose the version and get our index
  return id >> 32;
}
inline bool IsEntityValid(EntityID id)
{
  // Check if the index is our invalid index
  return (EntityIndex)id != EntityIndex(-1);
}

struct ComponentPool {
        char* cData = nullptr;
        size_t elementSize = 0;
    public:
        ComponentPool(size_t elementSize_) {
            elementSize = elementSize_;
            cData = new char[elementSize * MAX_COMP];
        };
        ~ComponentPool() {
            delete[] cData;
        }
        inline void* Get(size_t index) {
            if (index > MAX_COMP) return nullptr;
            return cData + index * elementSize;
        }
};

struct Scene {
        struct Entity {
            EntityID id;
            CompomentMask mask;
        };
        std::vector<Entity> entities{};
        std::vector<EntityIndex> freeEntities{};
        std::vector<ComponentPool*> ComponentPools{};
        EntityVersion newIndex = 0;
        EntityID NewEntity() {
            if (!freeEntities.empty()) {
                newIndex = freeEntities.back();
                freeEntities.pop_back();
                EntityID newID = CreateEntityId(newIndex, GetEntityVersion(entities[newIndex].id));
                entities[newIndex].id = newID;
                return entities[newIndex].id; 
            }
            entities.push_back({CreateEntityId(entities.size(),0),CompomentMask()});
            return entities.back().id;
        }
        void DestoryEntity(EntityID id) {
            if (GetEntityIndex(id) == EntityIndex(-1)) {
                spdlog::error("Entity id was incorrect");
                return;
            }
            EntityID newID = CreateEntityId(EntityIndex(-1), GetEntityVersion(id) + 1);
            entities[GetEntityIndex(id)].id = newID;
            entities[GetEntityIndex(id)].mask.reset(); 
            freeEntities.push_back(GetEntityIndex(id));
        }
        template <typename T> T* AssignCompoment(EntityID id) {
            if (entities[GetEntityIndex(id)].id != id) spdlog::error("You are accessing deleted entity!");
            int componentId = GetID<T>();
            if (ComponentPools.size() <= componentId) ComponentPools.resize(componentId + 1, nullptr);
            if (ComponentPools[componentId] == nullptr) ComponentPools[componentId] = new ComponentPool(sizeof(T));
            T* pComponent = new (ComponentPools[componentId]->Get(GetEntityIndex(id))) T();
            entities[GetEntityIndex(id)].mask.set(componentId); // SUS
            return pComponent;
        }
        template <typename T> T* GetCompoment(EntityID id) {
            if (entities[GetEntityIndex(id)].id != id) spdlog::error("You are accessing deleted entity!");
            int componentId = GetID<T>();
            if (!entities[GetEntityIndex(id)].mask.test(componentId)) {
                // spdlog::error("Something went really wrong! or you tried to access component that don't exits :)");
                return nullptr; 
            }
            T* pComponent = static_cast<T*>(ComponentPools[componentId]->Get(GetEntityIndex(id)));
            return pComponent;
        }
        template <typename T> void RemoveCompoment(EntityID id) {
            if (entities[GetEntityIndex(id)].id != id) { 
                spdlog::error("You are accessing deleted entity!");
                return;
            }
            int componentId = GetID<T>();
            entities[GetEntityIndex(id)].mask.reset(componentId);
        }
        template <typename T> inline bool CheckForComponent(EntityID id) {
            int componentId = GetID<T>();
            return entities[GetEntityIndex(id)].mask.test(componentId);
        }
};


template <typename... ComponentTypes>
class SceneView { // Check required
    private:
        bool all = false;
        CompomentMask componentMask;
        Scene *pScene;
    public:
        SceneView(Scene *pScene_) : pScene(pScene_) {
            if (sizeof...(ComponentTypes) == 0) {
                all = true;
            } else {
                int componentIds[] = {0, GetID<ComponentTypes>()... };
                for (int i=1; i<(sizeof...(ComponentTypes) + 1); i++) componentMask.set(componentIds[i]);
            }
        }
        class Iterator {
            EntityIndex index;
            Scene* pScene;
            CompomentMask mask;
            bool all = false;
            bool ValidIndex() {
                return IsEntityValid(pScene->entities[index].id) && 
                (all || mask == (mask & pScene->entities[index].mask));
            }
            public:
                Iterator(Scene* pScene, EntityIndex index, CompomentMask mask, bool all) : pScene(pScene), index(index), mask(mask), all(all) {}
                EntityID operator*() const {
                    return pScene->entities[index].id;
                }
                bool operator==(const Iterator& other) const {
                    return index == other.index || index == pScene->entities.size();
                };
                bool operator!=(const Iterator& other) const {
                    return index != other.index && index != pScene->entities.size();
                }
                Iterator& operator++() {
                    do { index++; } while (index < pScene->entities.size() && !ValidIndex());
                    return *this;
                }
        };
        const Iterator begin() const { // BIG sus
            int firstIndex = 0;
            while(firstIndex < pScene->entities.size() && (componentMask != (componentMask & pScene->entities[firstIndex].mask) || !IsEntityValid(pScene->entities[firstIndex].id))) { // Big SUS
                firstIndex++;
            } // To check for first index where is "active" entity present
            return Iterator(pScene, firstIndex, componentMask, all);
        };
        const Iterator end() const {
            return Iterator(pScene, EntityIndex(pScene->entities.size()), componentMask, all);
        }
};

#define INVALID_ENTITY CreateEntityId(EntityIndex(-1), 0)
