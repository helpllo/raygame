#include "utilities.hpp"

bool CheckCollisionRecsEqual(Rectangle rec1, Rectangle rec2) {
    bool collision = false;
    if ((rec1.x <= (rec2.x + rec2.width) && (rec1.x + rec1.width) >= rec2.x) &&
        (rec1.y <= (rec2.y + rec2.height) && (rec1.y + rec1.height) >= rec2.y)) collision = true;
    return collision;
}

bool CheckCollisionRecTringle(Rectangle rec1, Vector2 q, Vector2 w, Vector2 e)  {
    if(CheckCollisionPointTriangle(Vector2{rec1.x, rec1.y},q,w,e)) return true; 
    if(CheckCollisionPointTriangle(Vector2{rec1.x + rec1.width, rec1.y},q,w,e)) return true; 
    if(CheckCollisionPointTriangle(Vector2{rec1.x, rec1.y + rec1.height},q,w,e)) return true; 
    if(CheckCollisionPointTriangle(Vector2{rec1.x + rec1.width, rec1.y + rec1.height},q,w,e)) return true; 
    return false;
}
