#pragma once
// #include <queue>
#include "raylib.h"

enum EventType {
    PLAYER_COLLISION_GROUND,
    PLAYER_COLLISION_BOUNCY,
    PLAYER_COLLISION_FLAG,
    PLAYER_LOST_COLLISION,
    LEVEL_RELOAD,
};
/*
class Event {
    public:
        int what;
        const char* extra;
        Event(int what_, const char* extra_);
};

class EventManager {
    private:
        std::queue<Event> events;
    public:
        bool IsEventListEmpty();
        Event GetEvent();
        void SendEvent(Event event);
}; */

class Base {
    protected:
        // static inline EventManager events = EventManager();
        Vector3 position={0,0,0}; Vector2 scale={0,0};
        char* name;
    public:
        virtual void Update()=0;
        virtual void FixedUpdate()=0;
        virtual void EnterCollision(Base *collsion)=0;
        virtual void ExitCollision(Base *collsion)=0;
        virtual void StayCollision(Base *collision)=0;
};
