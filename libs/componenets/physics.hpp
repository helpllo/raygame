#pragma once
#include "raylib.h"
#include "ecs_core.hpp"
// Force sources
struct Gravity {
    const Vector2 gravity{0,9.81};
};

struct MovementInput {
    Vector2 power;
};

// Basic physics components
struct RigidBody {
    EntityID ent_collision;
    Vector2 speed_at_collision;
    bool is_collision=false;
    Vector2 size;
    Vector2 accelaration;
    Vector2 velocity;
    float weight;
};
