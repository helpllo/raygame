#include "raylib.h"
#include <vector>

struct Depth {
    int layer = 0;
};

struct Sprite {
    float height=0,width=0;
    Color color;
};

struct TextureSprite {
    float height=0,width=0;
    float rotation=0;
    Texture2D tex;
};

struct AnimatedSprite {
    float height=0,width=0;
    float rotation=0;
    float fps=60;
    std::vector<Texture2D> sprites;
};
