#pragma once
#include "raylib.h"
#include "config.h"
#include "base.hpp"
// #include <vector>

class Wall : public Base {
    private:
        // display
        Rectangle box{0,0,0,0};
        int x_off = 0, y_off = 0;
        float rotation = 0;
        Texture2D *texture;
        // logic
        int type;
        // physics
        float friction_x = 6/PHYSICS_TICKS*(60/PHYSICS_TICKS);
        float friction_y = GRAVITY;
    protected:
        friend class Game;
        void ChangePosition(Vector2 position);
        void ChangePosition(int x, int y);
    public:
        Wall(float x, float y, float w, float h, int type_, Texture2D *texture_, float rotation_=0);
        void Update();
        void FixedUpdate();
        void EnterCollision(Base *collision);
        void ExitCollision(Base *collision);
        void StayCollision(Base *collision);
        int GetType();
        Vector2 GetFriction();
        Rectangle GetRectangle();
};
