#include "physics.hpp"
#include "spdlog/spdlog.h"
#include "utilities.hpp"

void physics::update(const double dt, Scene *scene) {
    for (EntityID x : SceneView<RigidBody,Gravity>(scene)) {
        if (!scene->GetCompoment<RigidBody>(x)->is_collision) scene->GetCompoment<RigidBody>(x)->accelaration = Vector2Add(Vector2Zero(),scene->GetCompoment<Gravity>(x)->gravity);
    }
    for (EntityID x : SceneView<RigidBody>(scene)) {
        if (scene->GetCompoment<RigidBody>(x)->is_collision) {
            // scene->GetComsssssoment<RigidBody>(x)->velocity = Vector2Zero();
            scene->GetCompoment<RigidBody>(x)->accelaration =  Vector2Zero(); //Vector2Subtract(scene->GetCompoment<RigidBody>(x)->accelaration,scene->GetCompoment<RigidBody>(x)->accelaration);
        }
    }
    for (EntityID x : SceneView<Position,RigidBody>(scene)) {
        scene->GetCompoment<RigidBody>(x)->velocity =  Vector2Add(scene->GetCompoment<RigidBody>(x)->velocity,scene->GetCompoment<RigidBody>(x)->accelaration);        
        scene->GetCompoment<Position>(x)->position.y += scene->GetCompoment<RigidBody>(x)->velocity.y * dt;// * (GetMonitorPhysicalHeight(GetCurrentMonitor()) / float(GetMonitorHeight(GetCurrentMonitor())) / 500);
    }
}

void physics::update_rigidbodies(const double dt, Scene *scene) {
    for (EntityID x : SceneView<RigidBody,Position>(scene)) {
        for (EntityID y : SceneView<RigidBody,Position>(scene)) {
            if (x != y) {
                RigidBody *rigid_x = scene->GetCompoment<RigidBody>(x);
                Position *pos_x = scene->GetCompoment<Position>(x);
                RigidBody *rigid_y = scene->GetCompoment<RigidBody>(y);
                Position *pos_y = scene->GetCompoment<Position>(y);
                // spdlog::info("I got entity {} and entity {}",x,y);
                if (CheckCollisionRecsEqual(Rectangle{pos_x->position.x,pos_x->position.y,rigid_x->size.x,rigid_x->size.y},Rectangle{pos_y->position.x,pos_y->position.y,rigid_y->size.x,rigid_y->size.y})) {
                    rigid_y->is_collision = true;
                    rigid_x->is_collision = true;
                } 
                // else {
                //     rigid_y->colission = false;
                //     rigid_x->colission = false;
                // }
            }
        }
    } 
}
