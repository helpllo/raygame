#include "systems/render.hpp"
#include "componenets/sprite.hpp"
#include "componenets/base.hpp"

/* void render::SolidRender::update(const double time, Scene *scene) {
    for (EntityID x : SceneView<Sprite,Position>(scene)) {
        DrawRectangleV(scene->GetCompoment<Position>(x)->position,Vector2{scene->GetCompoment<Sprite>(x)->width,scene->GetCompoment<Sprite>(x)->height},scene->GetCompoment<Sprite>(x)->color);
    }
}
void render::TextureRender::update(const double time, Scene *scene) {
    for (EntityID x : SceneView<TextureSprite,Position>(scene)) {
        TextureSprite *tex = scene->GetCompoment<TextureSprite>(x);
        Position *pos = scene->GetCompoment<Position>(x);
        DrawTexturePro(tex->tex, Rectangle{0,0,float(tex->tex.width),float(tex->tex.height)}, Rectangle{pos->position.x,pos->position.y,tex->height,tex->width}, Vector2{0,0}, tex->rotation, BLACK);
    }
} */

void SortAndRun(Scene *scene) {
    std::array<std::vector<EntityID>,64> rendering_order;
    for (EntityID x : SceneView<Position,Depth>(scene)) {
        rendering_order.at(scene->GetCompoment<Depth>(x)->layer).push_back(x);
    }
    for (std::vector<EntityID> x : rendering_order) {
        for (EntityID y : x) {
            if (scene->CheckForComponent<Sprite>(y)) {
                DrawRectangleV(scene->GetCompoment<Position>(y)->position,Vector2{scene->GetCompoment<Sprite>(y)->width,scene->GetCompoment<Sprite>(y)->height},scene->GetCompoment<Sprite>(y)->color);
            } else if (scene->CheckForComponent<TextureSprite>(y)) {
                TextureSprite *tex = scene->GetCompoment<TextureSprite>(y);
                Position *pos = scene->GetCompoment<Position>(y);
                DrawTexturePro(tex->tex, Rectangle{0,0,float(tex->tex.width),float(tex->tex.height)}, Rectangle{pos->position.x,pos->position.y,tex->height,tex->width}, Vector2{0,0}, tex->rotation, WHITE);
            }
        }
    }
}
