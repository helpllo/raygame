#pragma once
#include "ecs_core.hpp"
#include "componenets/physics.hpp"
#include "componenets/base.hpp"
#include "raymath.h"

namespace physics {
        void update(const double time, Scene *scene);
        void update_rigidbodies(const double dt, Scene *scene);
}
