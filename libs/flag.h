#include <raylib.h>

class Flag {
    private:
        Rectangle box{0,0,20,20};
        Color color = GREEN;
    protected:
        friend class Game;
        void ChangePosition(int x, int y);
        void ChangePosition(Vector2 position);
    public:
        Rectangle GetRectangle();
        void Draw();
};
