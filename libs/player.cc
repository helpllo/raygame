#include "player.h"
#include <iostream>

Player::Player(const char* texture, float cox, float coy) {
    tex = LoadTexture(texture);
    // jump = LoadSound("resources/sounds/jump.wav");
    camera_offset_x = cox;
    camera_offset_y = coy;
    camera.zoom = 1.0;
    camera.rotation = 0;
    camera.offset = (Vector2){cox,coy};
}

Rectangle Player::GetRectangle() {
    return box;
}

void Player::Update() {
    if (ground) DrawLine(box.x + box.width / 2, box.y + box.height / 2, box.x - camera_offset_x + GetMouseX() + 10, box.y - camera_offset_y + GetMouseY() + 10, BLACK);
    DrawTexturePro(tex, Rectangle{0,0,0,0}, box, Vector2{0,0}, 0, WHITE);
    camera.target = (Vector2){box.x+10,box.y+10};
}

void Player::FixedUpdate() {
    // getting power of shot
    x_power = camera_offset_x - GetMouseX();  
    y_power = camera_offset_y - GetMouseY();
    angle = Vector2LineAngle(Vector2{box.x + 10, box.y + 10},Vector2{box.x+box.width/2 - camera_offset_x + GetMouseX(), box.y+box.height/2 - camera_offset_y + GetMouseY()});
    if (IsMouseButtonDown(MOUSE_LEFT_BUTTON) && ground)  {
        aimingPower = sqrt(pow(x_power, 2) + pow(y_power, 2));
        speed_x = (cos(angle)*aimingPower*3)/PHYSICS_TICKS;
        speed_y = (-sin(angle)*aimingPower*3)/PHYSICS_TICKS;
        // PlaySound(jump);
    }
    // handling collision
    if (collisions != nullptr) {
        if (CheckCollisionRecsEqual(collisions->GetRectangle(),Rectangle{box.x + speed_x, box.y, box.width, box.height})) {
            std::cout << "fff" << std::endl;
            Rectangle area = GetCollisionRec(collisions->GetRectangle(), Rectangle{box.x + speed_x, box.y, box.width, box.height});
            if (speed_x < -0.1) box.x = collisions->GetRectangle().x + collisions->GetRectangle().width;
            if (speed_x > 0.1) box.x = collisions->GetRectangle().x - box.width; // SUS
            if (!(area.height <= 3 && collisions->GetRectangle().y > box.y)) speed_x = 0;
        }
        if (CheckCollisionRecsEqual(collisions->GetRectangle(),Rectangle{box.x, box.y + speed_y, box.width, box.height})) {
            std::cout << "ddd" << std::endl;
            if (speed_y < -0.1) box.y = collisions->GetRectangle().y + collisions->GetRectangle().height;
            if (speed_y > 0.1) box.y = collisions->GetRectangle().y - box.height;
            speed_y = 0;
        }
    }
    // adding speed sanic
    box.x += speed_x;
    box.y += speed_y;
    // friction (not realistic but fun game wise)
    if (ground && (speed_x < -0.01 || speed_x > 0.01)) speed_x -= (speed_x/fabs(speed_x))*friction.x;
    else if (ground && (speed_x > -0.01 || speed_x < 0.01)) { speed_x = 0; }
    // if (ground && (speed_y < -0.1 || speed_y > 0.1)) speed_y -= (speed_y/fabs(speed_y))*friction.y;
    speed_y += GRAVITY;
    if (ground) speed_y -= friction.y;
    //else if (ground && (speed_y > -0.1 || speed_y < 0.1)) { speed_y = 0; }
}

void Player::EnterCollision(Base *collision) {
    Wall *wall = dynamic_cast<Wall*>(collision);
    if (wall == nullptr) { return; }
    std::cout << "Hello" << std::endl;
    switch(wall->GetType()) {
        case SOLID_WALL: {
            ground = true;
            friction = wall->GetFriction();
            /* speed_x = 0;
            speed_y = 0; */
            collision = wall;
            break;
        }
        case BOOSTER_JUMP: {
            speed_y = -5;
            break;
        }
    }
}

void Player::ExitCollision(Base *collision) {
    Wall *wall = dynamic_cast<Wall*>(collision);
    if (wall == nullptr) { return; }
    std::cout << "Goodbye" << std::endl;
    switch(wall->GetType()) {
        case SOLID_WALL: {
            ground = false;
            collision = nullptr;
            if (CheckCollisionRecs(wall->GetRectangle(),Rectangle{box.x + speed_x, box.y, box.width, box.height})) {
                std::cout << "fff" << std::endl;
                Rectangle area = GetCollisionRec(wall->GetRectangle(), Rectangle{box.x + speed_x, box.y, box.width, box.height});
                if (speed_x < -0.1) box.x = wall->GetRectangle().x + wall->GetRectangle().width;
                if (speed_x > 0.1) box.x = wall->GetRectangle().x - box.width; // SUS
                if (!(area.height <= 3 && wall->GetRectangle().y > box.y)) speed_x = 0;
            }
            if (CheckCollisionRecs(wall->GetRectangle(),Rectangle{box.x, box.y + speed_y, box.width, box.height})) {
                std::cout << "ddd" << std::endl;
                if (speed_y < -0.1) box.y = wall->GetRectangle().y + wall->GetRectangle().height;
                if (speed_y > 0.1) box.y = wall->GetRectangle().y - box.height;
                speed_y = 0;
            }
        }
        /* case BOOSTER_JUMP: {
            speed_y = -5;
            break;
        } */
    }
}

void Player::StayCollision(Base *collision) {
    Wall *wall = dynamic_cast<Wall*>(collision);
    if (wall == nullptr) { return; }
    std::cout << "ddd" << std::endl;
    /* if (CheckCollisionRecs(wall->GetRectangle(),Rectangle{box.x + speed_x, box.y, box.width, box.height})) {
        std::cout << "fff" << std::endl;
        Rectangle area = GetCollisionRec(wall->GetRectangle(), Rectangle{box.x + speed_x, box.y, box.width, box.height});
        if (speed_x < -0.1) box.x = wall->GetRectangle().x + wall->GetRectangle().width;
        if (speed_x > 0.1) box.x = wall->GetRectangle().x - box.width; // SUS
        if (!(area.height <= 3 && wall->GetRectangle().y > box.y)) speed_x = 0;
    } */
    if (CheckCollisionRecs(wall->GetRectangle(),Rectangle{box.x, box.y + speed_y, box.width, box.height})) {
        if (speed_y < -0.1) box.y = wall->GetRectangle().y + wall->GetRectangle().height;
        if (speed_y > 0.1) box.y = wall->GetRectangle().y - box.height;
        speed_y = 0;
    } 
}
void Player::Debug() {
    char x[50];
    sprintf(x, "Speed X: %f Speed Y: %f", speed_x, speed_y); 
    DrawText(x,30,70,15,BLACK);
    DrawText((ground ? "Grounded" : "Not Grounded"), 30, 90, 15, BLACK);
    sprintf(x, "Player X: %f Player Y: %f", box.x, box.y);
    DrawText(x,30,110,15,BLACK);
    /* sprintf(x, "Player angle: %f", direction_angle);
    DrawText(x,30,130,15,BLACK); */
}

Camera2D* Player::GetCamera() { return &camera; }

void Player::ChangePosition(int x, int y) {
    box.x = x;
    box.y = y;
}

void Player::ChangePosition(Vector2 position) {
    box.x = position.x;
    box.y = position.y;
}

void Player::CancelSpeed() {
    speed_x = 0;
    speed_y = 0;
}
