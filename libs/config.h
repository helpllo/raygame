#pragma once
const float PHYSICS_TICKS = 120;
const float GRAVITY = 9.81/PHYSICS_TICKS*(60/PHYSICS_TICKS); 

enum TypeOfWall {
    SOLID_WALL,
    SPIKE,
    BOOSTER_JUMP,
};

