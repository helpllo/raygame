#pragma once
#include <vector>
#include "yaml-cpp/yaml.h"
#include "raylib.h"
#include "wall.h"

class Level {
    public:
        std::string name;
        std::vector<Wall> walls;
        bool special = false;
        int custom_gravity = 0;
        Vector2 player_starting_position;
        Vector2 flag_position;
    public:
        Level(YAML::Node group, std::vector<Texture2D>* textures);
};
