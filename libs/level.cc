#include "level.h"
#include <iostream>


Level::Level(YAML::Node group, std::vector<Texture2D>* textures) {
    if(!group.IsMap()) std::cout << "something is wrong" << std::endl;
    special = group["special_rules"].as<bool>();
    name = group["name"].as<std::string>();
    if (special) custom_gravity = group["gravity"].as<int>();
    player_starting_position.x = group["player_spawn_position"][0].as<float>();
    player_starting_position.y = group["player_spawn_position"][1].as<float>();
    flag_position.x = group["flag_position"][0].as<float>();
    flag_position.y = group["flag_position"][1].as<float>();
    for (YAML::Node x : group["walls"]) {
        walls.push_back(Wall(x["position"][0].as<float>(),x["position"][1].as<float>(),x["position"][2].as<float>(),x["position"][3].as<float>(),x["type"].as<int>(),&textures->at(x["texture"].as<int>()),x["rotation"].as<float>()));
    }
}
