#pragma once
#include "config.h"
//#include "base.hpp"
#include "utilities.hpp"
#include "wall.h"
#include <raylib.h>
#include <raymath.h>
#include <sstream>
#include <string>
#include <vector>

class Player : public Base {
    private:
        // Input
        int x_power = 0, y_power = 0;
        float angle = 0;
        float direction_angle = 0;
        int aimingPower = 0;
        // physics
        float speed_x = 0, speed_y = 0;
        Vector2 friction = Vector2{0,0};
        bool ground = false;
        // visuals
        Rectangle box{0, 0, 20, 20};
        Texture2D tex;
        // Devil
        int camera_offset_x, camera_offset_y;
        Camera2D camera;
        Wall *collisions;
    public:
        Player(const char *texture, float camera_offset_x_, float camera_offset_y_);
        void Update();
        void FixedUpdate();
        void EnterCollision(Base *collision);
        void ExitCollision(Base *collision);
        void StayCollision(Base *collision);
        // Extra
        Rectangle GetRectangle();
        void Debug();
        Camera2D *GetCamera();
        void ChangePosition(int x, int y);
        void ChangePosition(Vector2 position);
        void CancelSpeed();
};
