#include "wall.h"

// protected
void Wall::ChangePosition(Vector2 position) {
    box.x = position.x;
    box.y = position.y;
}

void Wall::ChangePosition(int x, int y) {
    box.x = x;
    box.y = y;
}

// public
Wall::Wall(float x, float y, float w, float h, int type_, Texture2D *texture_, float rotation_) {
    box.x = x; box.y = y; box.width = w; box.height = h;
    type = type_; rotation = rotation_; texture = texture_;
    if (rotation == 90) { x_off = w; y_off = 0; }
    if (rotation == 180) { x_off = h; y_off = h; }
    if (rotation == 270) { x_off = 0; y_off = w; }
}

void Wall::Update() {
    int i;
    if (rotation == 180 || box.height <= box.width) {
        for (i=0; (i+1)*box.height<=box.width; i++) DrawTexturePro(*texture, Rectangle{0, 0, float(texture->width),float(texture->height)}, Rectangle{box.x+i*box.height+x_off,box.y+y_off,box.height,box.height} , Vector2{0,0}, rotation, WHITE);
        DrawTexturePro(*texture, Rectangle{0, 0, float(texture->width),float(texture->height)}, Rectangle{box.x+i*box.height + x_off,box.y + y_off,box.width - i*box.height, box.height} , Vector2{0,0}, rotation, WHITE);
    }
    if (rotation == 90 || rotation == 270 || box.height > box.width) {
        for (i=0; (i+1)*box.width<=box.height; i++) DrawTexturePro(*texture, Rectangle{0, 0, float(texture->width),float(texture->height)}, Rectangle{box.x + x_off,box.y+i*box.width+y_off,box.width,box.width} , Vector2{0,0}, rotation, WHITE);
        DrawTexturePro(*texture, Rectangle{0, 0, float(texture->width),float(texture->height)}, Rectangle{box.x + x_off,box.y+i*box.width+y_off,box.width,box.width - i*box.height} , Vector2{0,0}, rotation, WHITE);
    }
}

void Wall::FixedUpdate() {};
void Wall::EnterCollision(Base *collision) {}
void Wall::ExitCollision(Base *collision) {}
void Wall::StayCollision(Base *collision) {}
Vector2 Wall::GetFriction() { return Vector2{friction_x, friction_y}; }
int Wall::GetType() { return type; }
Rectangle Wall::GetRectangle() { return box; }
