#include "raylib.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ranges.h"
#include "ecs_core.hpp"
#include "systems/render.hpp"
#include "systems/physics.hpp"
// #include "componenets/base.hpp"
#include "componenets/sprite.hpp"
// #include "componenets/physics.hpp"
#include <random>
#include <cmath>
using namespace std;

int main() {
    // Scene lol;
    InitWindow(1280,720,"ECS demo");
    SetTargetFPS(100);
    float gravity = GetMonitorHeight(GetCurrentMonitor()) / GetMonitorPhysicalHeight(GetCurrentMonitor());
    spdlog::info("Current gravity it's {}",gravity);
    float velocity = 0;
    float accelaration = 0;
    Rectangle box;
    box.height = 10;
    box.width = 10;
    box.x = 10;
    box.y = 10;
    Rectangle platform;
    platform.height = 20;
    platform.width = 100;
    platform.x = 10;
    platform.y = 200;
    double dt = 1/100.0;
    double accumulator = 0;
    while(!WindowShouldClose()) {
        accumulator += GetFrameTime();
        BeginDrawing();
        ClearBackground(BLACK);
        DrawRectangleRec(box,BLUE);
        DrawRectangleRec(platform,WHITE);
        DrawFPS(10,320);
        EndDrawing();
        if (accumulator >= dt) { 
            accelaration = gravity;
            if (IsKeyPressed(KEY_SPACE)) {
                accelaration -= 9;
            }
            if (CheckCollisionRecs(box, platform)) {
                accelaration = 0;
                velocity = 0;
            }
            velocity += accelaration;
            box.y += velocity * dt;
            accumulator -= dt;
        }
    }
    return 0;
}
