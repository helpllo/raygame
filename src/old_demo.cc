#include <iostream>
#include "utilities.hpp"
#include "cxxopts.hpp"

using namespace std;

class Number {
    public:
        int x=0,y=0;
};

class Object {
    protected:
        static Number *x;
    public:
        virtual int GetX()=0;
        virtual int GetY()=0;
        
};
Number Object::*x = NULL;
class Cow : public Object {
    public:
        int GetX() { return x->x; }
        int GetY() { return x->y; }
        void SetNumber(Number x_) {
            x = &x_;
        }
};

class Duck : public Object {
    public:
        int GetX() { return x->x + 1; }
        int GetY() { return x->y + 1; }
};

int main(int argc, char* argv[]) {
    Number a;
    Cow b;
    Duck c;
    a.x = 0;
    a.y = 0;
    b.SetNumber(a);
    cout << b.GetX() << b.GetY() << endl;
    cout << c.GetX() << c.GetY() << endl;
    a.x = 4;
    a.y = 5;
    b.SetNumber(a);
    cout << b.GetX() << b.GetY() << endl;
    cout << c.GetX() << c.GetY() << endl;
    return 0;
}
