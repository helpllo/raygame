#include "raylib.h"
#include "level.h"
#include "yaml-cpp/yaml.h"
#include "cxxopts.hpp"
#include <vector>
#include <fstream>

const int screen_w = 1280, screen_h = 720;
std::vector<Rectangle> rectangles;
Rectangle player{0,0,20,20};
Rectangle flag{20,0,20,20};

int main(int argc, char* argv[]) {
    cxxopts::Options options("Game editor","Tool to make maps for my game");
    options.add_options()
        ("d,dry", "Don't write to file");
    auto results = options.parse(argc, argv);
    bool wasPressed = false, modeFlags = false;
    unsigned int gridSize = 20;
    float flx = 0, slx = 0;
    float fly = 0, sly = 0;
    float new_x = 0, new_y = 0;
    InitWindow(screen_w,screen_h,"Map editor");
    SetTargetFPS(300);
    while (!WindowShouldClose()) {
        BeginDrawing();
        ClearBackground(BLACK);
        for (int i=0; i<screen_w; i+=gridSize) DrawLine(i,0,i,screen_h,Color{128,128,128,255});
        for (int i=0; i<screen_h; i+=gridSize) DrawLine(0,i,screen_w,i,Color{128,128,128,255});
        if (!modeFlags) {
            if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
                if (wasPressed) {
                    slx = GetMouseX()/gridSize*gridSize;
                    sly = GetMouseY()/gridSize*gridSize;
                    rectangles.push_back(Rectangle{flx,fly,((slx - flx)/gridSize)*gridSize,((sly - fly)/gridSize)*gridSize});
                    wasPressed = false;
                } else {
                    flx = GetMouseX()/gridSize*gridSize;
                    fly = GetMouseY()/gridSize*gridSize;
                    wasPressed = true;
                }
            }
        } else {
            if(IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {
                new_x = GetMouseX()/gridSize*gridSize;
                new_y = GetMouseY()/gridSize*gridSize;
                player.x = new_x;
                player.y = new_y;
            }
        }
        for (auto x : rectangles) {
            DrawRectangleRec(x,WHITE);
        }
        DrawRectangleRec(player,BLUE);
        DrawRectangleRec(flag,GREEN);
        DrawFPS(0,0);
        EndDrawing();
        if (IsKeyPressed(KEY_EQUAL)) gridSize += 5;
        if (IsKeyPressed(KEY_MINUS)) gridSize -= 5;
        if (IsKeyPressed(KEY_F)) { 
            if (!wasPressed) { modeFlags = true; wasPressed = true; } 
            else { modeFlags = false; wasPressed = false; }
        }
    }
    CloseWindow(); std::string buffer;
    if (!results["dry"].as<bool>()) {
        YAML::Emitter ext;
        ext << YAML::BeginSeq << YAML::BeginMap;
        ext << YAML::Key << "name" << YAML::Value << "something";
        ext << YAML::Key << "special_rules" << YAML::Value << false;
        ext << YAML::Key << "player_spawn_position" << YAML::Value << YAML::Flow << YAML::BeginSeq << 0 << 0 << YAML::EndSeq;
        ext << YAML::Key << "flag_position" << YAML::Value << YAML::Flow << YAML::BeginSeq << 200 << 0 << YAML::EndSeq;
        ext << YAML::Key << "walls" << YAML::Value << YAML::BeginSeq;
        for (auto x : rectangles) {
            ext << YAML::BeginMap;
            ext << YAML::Key << "position" << YAML::Value << YAML::Flow << YAML::BeginSeq << x.x << x.y << x.width << x.height << YAML::EndSeq;
            ext << YAML::Key << "type" << YAML::Value << 0;
            ext << YAML::Key << "rotation" << YAML::Value << 0;
            ext << YAML::Key << "texture" << YAML::Value << 0;
            ext << YAML::EndMap;
        }
        std::fstream file;
        file.open("editor.yml",std::fstream::out);
        file << ext.c_str();
        file.close();
    }
}
