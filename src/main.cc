// Externial dependencies
#include "raylib.h"
#include "yaml-cpp/yaml.h"
#include "raymath.h"
#include "cxxopts.hpp"
#include "spdlog/spdlog.h"
// standard c++ library
#include <vector>
#include <chrono>
#include <thread>
#include <cmath>
#include <cstdlib>
#include <iostream>
// My classes
#include "player.h"
#include "flag.h"
#include "level.h"
#include "utilities.hpp"

using namespace std;

// Configuration
const int screen_w = 1280;
const int screen_h = 720;
const int targeted_fps = 300;
// Phycics
const float physics_ticks = PHYSICS_TICKS;
const float gravity = 9.81/physics_ticks*(60/physics_ticks);
const long time_tick = ceil((1.0 / physics_ticks) * pow(10, 3));

// Other staff
Camera2D camera;
std::vector<Texture2D> list_of_textures;

class Game {
    private:
        Wall *last_collision = NULL;
        int level = 0;
        Player *player;
        Flag flag;
        std::vector<Wall> walls;
        std::vector<Level> levels;
        bool win_condiction = false;
        // int points;
    public:
        Game(Player *player_, string levels_file, int starting_level) {
            if (player_ == NULL) cerr << "player_ is NULL!" << endl;
            player = player_;
            camera.zoom = 1.0f;
            camera.rotation = 0;
            camera.offset = (Vector2){screen_w/2.0f, screen_h/2.0f};
            level = starting_level;
            YAML::Node root = YAML::LoadFile(levels_file);
            for (auto d : root) levels.push_back(Level(d,&list_of_textures));
            LoadLevel();
        }
        void setLevel(int level_) {
            level = level_;
            win_condiction = false; // SUS line
            LoadLevel();
        }
        void LoadLevel(int level_=-1) {
            if (level_ == -1) level_ = level;
            else level = level_;
            if (win_condiction) level++;
            walls.clear();
            walls = levels.at(level).walls;
            flag.ChangePosition(levels.at(level).flag_position);
            player->ChangePosition(levels.at(level).player_starting_position);
            win_condiction = false;
            last_collision = nullptr;
            return;
        }
        void CheckForCollisions() {
            for (auto wall : walls) {
                if(CheckCollisionRecsEqual(wall.GetRectangle(), player->GetRectangle())) {
                    if (&wall != last_collision) {
                        last_collision = &wall;
                        player->EnterCollision(&wall);
                        wall.EnterCollision(player);
                    } else {
                        player->StayCollision(&wall);
                        wall.StayCollision(player);
                    }
                } else if (last_collision != NULL && !CheckCollisionRecsEqual(last_collision->box, player->GetRectangle())) {
                    player->ExitCollision(last_collision);
                    last_collision->ExitCollision(player);
                    last_collision = NULL;
                }
            }
            if (CheckCollisionRecs(player->GetRectangle(),flag.GetRectangle())) {
                win_condiction = true;
                LoadLevel();
            }
            /* for (auto x : walls) {
                if (CheckCollisionRecsEqual(player->GetRectangle(), x.GetRectangle())) { 
                    if (x.GetType() == SPIKE) LoadLevel();
                    player->HandleCollision(true, &x); 
                    break; 
                }
                else player->HandleCollision(false, &x);
            } */
            camera.target = Vector2{player->GetRectangle().x+10,player->GetRectangle().y+10};
        }
        Player* getPlayer() { return player; };
        Camera2D* getCamera() { return &camera; };
        Flag* getFlag() { return &flag; };
        std::vector<Wall>* getWalls() { return &walls; };
};

void handle_physics(Game *game) {
    while (!WindowShouldClose()) {
        game->getPlayer()->FixedUpdate();
        game->CheckForCollisions();
        this_thread::sleep_for(chrono::milliseconds(time_tick));
    }
}

int main(int argc, char* argv[]) {
    cxxopts::Options options("Game", "Game itself");
    options.add_options()
        ("f,levels", "Load custom level file", cxxopts::value<std::string>()->default_value("resources/levels.yml"))
        ("l,starting_level", "Set starting level", cxxopts::value<int>()->default_value("0"));
    auto results = options.parse(argc, argv);
    // Let's get started 
    InitWindow(screen_w, screen_h, "Hello... game");
    SetTargetFPS(targeted_fps);
    Texture2D n = LoadTexture("resources/grass_no.png"); list_of_textures.push_back(n);
    Texture2D s = LoadTexture("resources/kolec.png"); list_of_textures.push_back(s);
    Texture2D z = LoadTexture("resources/życko.png"); list_of_textures.push_back(z);
    Texture2D w = LoadTexture("resources/ściana.png"); list_of_textures.push_back(w);
    // Texture2D background = LoadTexture("resources/background.png");
    Player player("resources/gracz.png", screen_w/2, screen_h/2);
    Game game(&player,results["levels"].as<string>(),results["starting_level"].as<int>());
    // For debuging
    char buffer[50];
    // It's time.
    thread physics(handle_physics, &game); // This thread is resposible for physics
    while (!WindowShouldClose()) {
        // Drawing
        BeginDrawing(); // just send 'images' to screen
            ClearBackground(BLACK);
        BeginMode2D(*game.getCamera()); // moves with camera
            game.getFlag()->Draw();
            player.Update();
            for(int i=0; i<game.getWalls()->size(); i++) game.getWalls()->at(i).Update();
        EndMode2D();
            sprintf(buffer, "Camara X: %f Camera Y: %f", camera.target.x, camera.target.y);
            DrawText(buffer, 30, 30, 15, BLACK);
            player.Debug();
        EndDrawing(); // finish ending 'images' to screen and poll our input events
        // Controls 
        if (IsKeyDown(KEY_R)) game.LoadLevel(); // This is quite usefull
    }
    physics.join();
    CloseWindow();
    return 0;
}
